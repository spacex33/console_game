package ua.epam.training.config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Class that represents work with property files.
 */
public class PropertyValue {

    /**
     * Class that represents properties
     */
    private Properties prop = new Properties();

    /**
     * Properties file name constant
     */
    private static final String PROPERTIES_FILE_NAME = "game.properties";

    /**
     * Returns property by key
     *
     * @param key key
     * @return property
     */
    public String getProperty(String key) {

        InputStream inputStream;

        try {
            inputStream = getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + PROPERTIES_FILE_NAME + "' not found in the classpath");
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return prop.getProperty(key);
    }
}
