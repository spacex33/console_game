package ua.epam.training.io;

/**
 * Interface-marker that indicates,
 * that we can treat to classes in parsing context.
 *
 * @author Sergey
 */
public interface Parseable {
}
