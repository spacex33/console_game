package ua.epam.training.io;

/**
 * Interface represents abstract method
 * that parses some Parseable entity from file.
 *
 * @author Sergey
 */
public interface Parser {

    /**
     * Parse entity that implements Parseable.
     *
     * @return returns instance that implements Parseable interface.
     */
    Parseable parseEntity();
}
