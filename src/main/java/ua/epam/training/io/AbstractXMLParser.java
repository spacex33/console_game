package ua.epam.training.io;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import ua.epam.training.config.PropertyValue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * Base abstract class for classes that parses entities from xml file.
 *
 * @author Sergey
 */
abstract class AbstractXMLParser implements Parser {

    /**
     * Gets file name for parsing from properties file.
     */
    private static final String FILE_NAME = new PropertyValue().getProperty("game_file_path");

    /**
     * Document instance which child classes uses for parsing.
     */
    protected final Document document = loadXml(FILE_NAME);

    /**
     * Creating Document instance from BuilderFactory.
     *
     * @param fileName name of xml file
     * @return Document instance
     */
    private Document loadXml(String fileName) {
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new File(fileName));
            doc.getDocumentElement().normalize();

            return doc;

        } catch (ParserConfigurationException | SAXException e) {
            throw new IllegalStateException("Error parsing the XML", e);
        } catch (IOException e) {
            throw new IllegalStateException("Error accessing the XML", e);
        }
    }
}
