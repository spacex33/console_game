package ua.epam.training.io;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import ua.epam.training.model.Effect;
import ua.epam.training.model.Options;
import ua.epam.training.model.Plot;
import ua.epam.training.model.Scene;

import java.util.*;

/**
 * Class used to retrieve data associated with plot from the xml file.
 *
 * @author Orest
 */
public class PlotXMLParser extends AbstractXMLParser {

    /**
     * Parses all data inside 'scenes' tag.
     *
     * @return Plot instance parsed from xml file
     */
    public Plot parseEntity() {
        Set<Scene> scenes;
        NodeList root = document.getElementsByTagName("scenes");

        Boolean randomScenesValue = Boolean.parseBoolean(
                root
                        .item(0)
                        .getAttributes()
                        .getNamedItem("random")
                        .getNodeValue()
        );

        if (randomScenesValue) {

            // if attribute random in xml file is true,
            // we use HashSet to return values in no particular order
            scenes = new HashSet<>();
        } else {
            scenes = new LinkedHashSet<>();
        }
        Node nodeScenes = root.item(0);
        if (nodeScenes.hasChildNodes()) {
            NodeList child = nodeScenes.getChildNodes();
            for (int i = 0; i < child.getLength(); i++) {
                Node c = child.item(i);
                if (c.getNodeName().equals("scene")) {
                    Scene s = createScene(c);
                    scenes.add(s);
                }
            }
        }
        return new Plot(scenes);
    }

    private void handleSceneChild(Node child, Scene scene) {
        switch (child.getNodeName()) {
            case "text":
                scene.setText(getText(child));
                break;
            case "options":
                scene.setOptions(getOptions(child));
                break;
        }

    }

    private void handleOptionsChild(Node node, Options option) {
        switch (node.getNodeName()) {
            case "text":
                option.setText(getText(node));
                break;
            case "effects":
                option.setEffects(getEffects(node));
                break;
        }
    }

    private void handleEffectChild(Node n, Effect effect) {
        switch (n.getNodeName()) {
            case "text":
                effect.setEffectName(getText(n));
                break;
            case "properties":
                effect.setEffectProperties(getProperties(n, effect));
                break;
        }
    }

    private Scene createScene(Node c) {
        Scene scene = new Scene();
        NodeList child = c.getChildNodes();
        for (int i = 0; i < child.getLength(); i++) {
            Node attr = child.item(i);
            handleSceneChild(attr, scene);
        }
        return scene;
    }

    private Options createOption(Node node) {
        Options option = new Options();
        if (node.hasChildNodes()) {
            NodeList optionChild = node.getChildNodes();
            for (int i = 0; i < optionChild.getLength(); i++) {
                Node child = optionChild.item(i);
                handleOptionsChild(child, option);
            }
        }
        return option;
    }

    private Effect createEffect(Node root) {
        Effect effect = new Effect();
        effect.setEffectProperties(new HashMap<String, String>());
        if (root.hasChildNodes()) {
            NodeList effects = root.getChildNodes();
            for (int i = 0; i < effects.getLength(); i++) {
                Node node = effects.item(i);
                handleEffectChild(node, effect);
            }
        }
        return effect;
    }

    private List<Options> getOptions(Node node) {
        LinkedList<Options> options = new LinkedList<>();
        if (node.hasChildNodes()) {
            NodeList child = node.getChildNodes();
            for (int i = 0; i < child.getLength(); i++) {
                Node c = child.item(i);
                if (c.getNodeName().equals("option")) {
                    options.addLast(createOption(c));
                }
            }
            return options;
        }
        return null;
    }

    private List<Effect> getEffects(Node root) {
        List<Effect> effects = new ArrayList<>();
        if (root.hasChildNodes()) {
            NodeList child = root.getChildNodes();
            for (int i = 0; i < child.getLength(); i++) {
                Node node = child.item(i);
                if (node.getNodeName().equals("effect"))
                    effects.add(createEffect(node));
            }
        }
        return effects;
    }

    private Map<String, String> getProperties(Node node, Effect effect) {
        Map<String, String> properties = new HashMap<>();
        if (node.hasChildNodes()) {
            NodeList p = node.getChildNodes();
            for (int i = 0; i < p.getLength(); i++) {
                Node property = p.item(i);
                if (property.getNodeName().equals("property")) {
                    getProperty(property, properties);
                }
            }
        }
        return properties;
    }

    private void getProperty(Node property, Map<String, String> properties) {
        String key = property.getAttributes().getNamedItem("type").getNodeValue();
        String value = property.getTextContent();
        properties.put(key, value);
    }

    private String getText(Node node) {
        return node.getTextContent();
    }
}