package ua.epam.training.io;

import org.w3c.dom.NodeList;
import ua.epam.training.model.Character;

import java.util.HashMap;
import java.util.Map;

/**
 * Class used to retrieve data associated with character from the xml file.
 *
 * @author Sergey
 * @since 2018-11-17
 */
public class CharacterXMLParser extends AbstractXMLParser{

    /**
     * Parses all data inside 'character' tag.
     *
     * @return Character instance parsed from xml file
     */
    public Character parseEntity() {
        Map<String, String> map = new HashMap<>();

        NodeList element = document.getElementsByTagName("character").item(0).getChildNodes();

        NodeList nodeList = element.item(1).getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {

            if (nodeList.item(i).getNodeName().equals("property")) {

                String key = nodeList.item(i).getAttributes().getNamedItem("type").getNodeValue();
                String value = nodeList.item(i).getTextContent();
                map.put(key, value);
            }

        }
        return new Character(map);
    }

}
