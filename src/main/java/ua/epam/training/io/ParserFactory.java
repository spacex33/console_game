package ua.epam.training.io;

import ua.epam.training.model.Character;
import ua.epam.training.model.Plot;

/**
 * A factory that encapsulates the creation of objects from parsing.
 */
public class ParserFactory {

    /**
     * Creates and returns Parseable implemented instance based on Class object.
     *
     * @param clazz Java Class object
     * @return class that implements Parseable interface
     */
    public static Parseable getEntity(Class clazz) {

        Parseable parser = null;

        if (clazz.getName().equals(Character.class.getName())) {
            parser = new CharacterXMLParser().parseEntity();

        } else if (clazz.getName().equals(Plot.class.getName())){
            parser = new PlotXMLParser().parseEntity();
        }
        return parser;
    }
}
