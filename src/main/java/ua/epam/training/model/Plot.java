package ua.epam.training.model;

import ua.epam.training.io.Parseable;

import java.util.Set;

public class Plot implements Parseable {

    private Set<Scene> scenes;

    public Plot(Set<Scene> scenes) {
        this.scenes = scenes;
    }

    public Set<Scene> getScenes() {
        return scenes;
    }

    public void setScenes(Set<Scene> scenes) {
        this.scenes = scenes;
    }
}
