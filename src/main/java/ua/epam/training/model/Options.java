package ua.epam.training.model;

import ua.epam.training.io.Parseable;

import java.util.List;

public class Options implements Parseable {

    private String text;
    private List<Effect> effects;

    /**
     * Constructor w/o parameters is required for
     * ability to create object by setters
     */
    public Options() {
    }

    public Options(String text, List<Effect> effects) {
        this.text = text;
        this.effects = effects;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Effect> getEffects() {
        return effects;
    }

    public void setEffects(List<Effect> effects) {
        this.effects = effects;
    }

    @Override
    public String toString() {
        return "Options{" +
                "text='" + text + '\'' +
                ", effects=" + effects +
                '}';
    }
}
