package ua.epam.training.model;

import ua.epam.training.controller.GameController;
import ua.epam.training.io.Parseable;
import ua.epam.training.io.ParserFactory;
import ua.epam.training.view.GameView;

import java.util.Map;

public class Character implements Parseable {

    private Map<String, String> oldProperties;
    private Map<String, String> properties;

    public Character(Map<String, String> properties) {
        this.properties = properties;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    @Override
    public String toString() {
        return "Character{" +
                "properties=" + properties +
                '}';
    }

    public String getPropertiesDifference(Effect effect) {
        StringBuilder sb = new StringBuilder();
        for (String key : effect.getEffectProperties().keySet()) {
            sb.append(this.properties.get(key));
            sb.append(effect.getEffectProperties().get(key));
            sb.append(" ");
        }
        String result = sb.toString().trim().replace(" ", " HEALTH: ");
        result = "MOOD: " + result;
        return result;
    }

    public void applyEffect(Effect effect) {
        this.oldProperties = this.properties;
        Map<String, String> effectProperties = effect.getEffectProperties();
        int i;
        for (String key : effectProperties.keySet()) {
            //effect on character goes here
            if (effectProperties.containsKey(key)) {
                i = Integer.parseInt(this.properties.get(key));
                i += Integer.parseInt(effectProperties.get(key));
                this.properties.replace(key, Integer.toString(i));
                if (i <= 0) {
                    System.out.println("=============================\n" +
                            "You died. Game over.\n" +
                            "=============================\n");
                    System.out.println("Would you like to try again?\n");
                    new GameController(
                            new GameView(),
                            (Character) ParserFactory.getEntity(Character.class),
                            (Plot) ParserFactory.getEntity(Plot.class))
                            .start();
                }
            }
        }
    }
}
