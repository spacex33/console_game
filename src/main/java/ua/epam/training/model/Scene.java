package ua.epam.training.model;

import ua.epam.training.io.Parseable;

import java.util.List;

public class Scene implements Parseable {

    private String text;
    private List<Options> options;

    /**
     * Constructor w/o parameters is required for
     * ability to create object by setters
     */
    public Scene() {
    }

    public Scene(String text, List<Options> options) {
        this.text = text;
        this.options = options;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Options> getOptions() {
        return options;
    }

    public void setOptions(List<Options> options) {
        this.options = options;
    }

    @Override
    public String toString() {
        return "Scene{" +
                "text='" + text + '\'' +
                ", options=" + options +
                '}';
    }

    public String getText() {
        return text;
    }
}
