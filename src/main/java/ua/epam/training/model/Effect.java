package ua.epam.training.model;

import ua.epam.training.io.Parseable;

import java.util.Map;

public class Effect implements Parseable {

    private String effectName;
    private Map<String, String> effectProperties;

    /**
     * Constructor w/o parameters is required for
     * ability to create object by setters
     */
    public Effect() {
    }

    public Effect(String effectName, Map<String, String> effectProperties) {
        this.effectName = effectName;
        this.effectProperties = effectProperties;
    }

    public String getEffectName() {
        return effectName;
    }

    public void setEffectName(String effectName) {
        this.effectName = effectName;
    }

    public Map<String, String> getEffectProperties() {
        return effectProperties;
    }

    public void setEffectProperties(Map<String, String> effectProperties) {
        this.effectProperties = effectProperties;
    }

    @Override
    public String toString() {
        return "Effect{" +
                "effectName='" + effectName + '\'' +
                ", effectProperties=" + effectProperties +
                '}';
    }
}
