package ua.epam.training;

import ua.epam.training.controller.GameController;
import ua.epam.training.io.ParserFactory;
import ua.epam.training.model.Character;
import ua.epam.training.model.Plot;
import ua.epam.training.view.GameView;

/**
 * Main class, that contains an entry point of application.
 *
 * @author Sergey
 * @since 2018-11-16
 */
public class Main {
    public static void main(String[] args) {
        new GameController(
                new GameView(),
                (Character) ParserFactory.getEntity(Character.class),
                (Plot) ParserFactory.getEntity(Plot.class))
                .start();
    }
}
