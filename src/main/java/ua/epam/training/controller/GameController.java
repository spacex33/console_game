package ua.epam.training.controller;

import ua.epam.training.model.Character;
import ua.epam.training.model.*;
import ua.epam.training.view.GameView;

import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Main game controller in Model-View-Controller pattern.
 * Its primary job is to set up the stage for the view
 * to interact with the model.
 *
 * @author Sergey
 */
public class GameController {

    /**
     * Used to display data for user.
     */
    private GameView gameView;

    /**
     * Represents main character entity
     */
    private Character character;

    /**
     * Collection of Scene entities
     */
    private Set<Scene> sceneSet;

    public GameController(GameView gameView, Character character, Plot plot) {
        this.gameView = gameView;
        this.character = character;
        this.sceneSet = plot.getScenes();
    }

    public void start() {
        while (true) {

            showMainMenu();

        }
    }

    private void startGame() {
        for (Scene s : sceneSet) {
            gameView.printSceneDesc(s);
            gameView.printSceneOptions(s);

            //choice and effects on character
            effectOnCharacter(userChoice(s));
        }
        gameView.print("=============================\n" +
                "You managed to survive. Game over.\n" +
                "=============================\n");
        showMainMenu();
    }

    private Options userChoice(Scene s) {
        List<Options> options = s.getOptions();
        int optInput = gameView.getInput();
        while (optInput < 1 || optInput > options.size()) {
            gameView.print("You entered an incorrect number. \n" +
                    "Please, enter number from 1 to " + options.size() + "\n");
            gameView.printSceneOptions(s);
            optInput = gameView.getInput();
        }
        return options.get(optInput - 1);
    }

    private void effectOnCharacter(Options option) {
        List<Effect> effects = option.getEffects();
        Effect effect = effects.get(new Random().nextInt(effects.size()));

        gameView.print(this.character.getPropertiesDifference(effect));
        this.character.applyEffect(effect);
        gameView.printEffectText(effect);
    }


    private void showMainMenu() {
        gameView.printMenu();
        int menuChoice = gameView.getInput();

        switch (menuChoice) {
            case 1:
                startGame();
                break;
            case 0:
                System.exit(0);
                break;
            default:
                while (menuChoice != 0 && menuChoice != 1) {
                    gameView.print("You entered an incorrect number. Try again");
                    gameView.printMenu();
                    menuChoice = gameView.getInput();
                }
        }
    }
}
