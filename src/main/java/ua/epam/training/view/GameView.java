package ua.epam.training.view;

import ua.epam.training.model.Effect;
import ua.epam.training.model.Options;
import ua.epam.training.model.Scene;

import java.util.List;

/**
 * Class represents main View in Model-View-Controller pattern.
 * Its job to create methods for displaying Models to the user,
 * and encapsulate work with the class that we use to display information.
 *
 * @author Sergey
 */
public class GameView {

    /**
     * Class that we use to display information for user.
     */
    private Console console = new Console();

    public void printSceneDesc(Scene scene) {
        console.print("\n" + scene.getText());
    }

    public int getInput() {
        return console.getInput();
    }

    public void print(String str) {
        console.print(str);
    }

    public void printMenu() {
        console.print("Welcome to the game! \n" +
                "1 - Start \n" +
                "0 - Exit");
    }

    public void printEffectText(Effect effect) {
        console.print(effect.getEffectName());
    }

    public void printSceneOptions(Scene s) {
        List<Options> options = s.getOptions();
        int i = 1;
        for (Options o : options) {
            console.print(i++ + " - " + o.getText());
        }
    }
}
